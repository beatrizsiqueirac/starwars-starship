import axios from "axios";

export const api = axios.create({
    baseURL: 'https://swapi.dev/api/',
});

export const getStarships = async (
) => {
    const apiResponse = await api.get("/starships/");
    return apiResponse.data;
};
