export const errorHandler = (e) => {
    if (e?.response?.data) {
        alert(e.response.data.message)
    } else {
        alert(e.message)
    }
}