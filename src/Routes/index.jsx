import { BrowserRouter, Route, Switch } from 'react-router-dom'
import NotFound from '../pages/messages/NotFound'
import Home from '../pages/User/Home'
import Answer from '../pages/User/Answer'

const Routes = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path='/' component={Home}></Route>
                <Route exact path='/answer' component={Answer}></Route>
                <Route component={NotFound}></Route>
            </Switch>
        </BrowserRouter>
    )
}

export default Routes;
