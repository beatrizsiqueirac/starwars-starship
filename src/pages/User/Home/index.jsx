import { useState } from 'react'
import { useHistory } from "react-router-dom";
import './style.css'
import logo from '../../../assets/logotipo.svg'

export default function Home() {

    const [mglt, setMglt] = useState('')   
    const history = useHistory();

    async function handleSubmit(event) {
        history.push({
            pathname: '/answer',
            state: { mglt: mglt }
          });
    }

    return (
        <div className="home-container" id='homeContainer'>
            <div className='form-home'>
                <img src={logo} alt='developer' className='icon'></img>
                <form onSubmit={handleSubmit}>
                    <h1>Welcome!</h1>
                    <h3>Input a MGLT value to find out</h3>
                    <h3>how many stops your next journey will make</h3>
                    <div className="input-container">
                        <div className='input-box'>
                            <input
                                placeholder="MGLT"
                                value={mglt}
                                required={true}
                                onChange={event => setMglt(event.target.value)}
                            />
                        </div>
                        <button type="submit" className='button'>Find out now!</button>
                    </div>
                </form>
            </div>
            </div >
    )
}

