import { useState,  useEffect } from 'react'
import '../../../css/forms.css'
import { getStarships } from '../../../services/api'
import { FiArrowLeft } from "react-icons/fi";
import { DataGrid } from '@material-ui/data-grid';
const columns = [
   
    { field: 'name', headerName: 'Name', width: 130 },
    { field: 'numberOfStops', headerName: 'Stops', width: 130 },
    
  ];
  

const UserCreate = (props) => {
    const {mglt} = props.location.state
    const [starships, setStarships] = useState()  

    const turnStringOnTime = (consumables) => {
        if(consumables.indexOf("month") !== -1 || consumables.indexOf("months") !== -1){
            const numbers = consumables.replace(/[^0-9]/g,'')            
            return parseInt(numbers) * 30 * 24;
        }
        if(consumables.indexOf("week") !== -1 || consumables.indexOf("weeks") !== -1){
            const numbers = consumables.replace(/[^0-9]/g,'')            
            return parseInt(numbers) * 7 * 24;
        }
        if(consumables.indexOf("day") !== -1 || consumables.indexOf("days") !== -1){
            const numbers = consumables.replace(/[^0-9]/g,'')            
            return parseInt(numbers) * 24;
        }
        if(consumables.indexOf("year") !== -1 || consumables.indexOf("years") !== -1){
            const numbers = consumables.replace(/[^0-9]/g,'')            
            return parseInt(numbers) * 365 * 24;
        }
        if(consumables.indexOf("hour") !== -1 || consumables.indexOf("hours") !== -1){
            const numbers = consumables.replace(/[^0-9]/g,'')            
            return parseInt(numbers);
        }
        return consumables;
    }

    
    const callStarships = async () => {
        const response = await getStarships()
        const starshipsObject = response?.results?.map((item, index) =>{
            const capacity = mglt / (item.MGLT * turnStringOnTime(item.consumables))
            const numberOfStops = Math.round(capacity)
            const id = `table-${index}`
            return {...item, numberOfStops, id}
        })
        setStarships(starshipsObject)
    }
    useEffect(() => {
        callStarships()        
    }, [])
    return (
        <div className="form-content">
        <button className="back-link" onClick={() => props.history.push('/')}>
                            <FiArrowLeft size={16} color="#eeff05" />
                    Voltar
                </button>
        <div className="form-content">
                    <section className="form-description">
                        <h1>Here is the number of stops for each starship</h1>
                        <p>
                            Choose one to start your journey!
                        </p>
                        
                </section>
                </div>
                <div style={{ height: 400, width: '100%' }} >
      <DataGrid rows={starships || []} columns={columns} pageSize={5} className="form-description" />
    </div>
                </div>

    );
};

export default UserCreate;
